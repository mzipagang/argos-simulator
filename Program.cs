﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SecurityToken;

namespace argos_simulator
{
    class Program
    {
        const string apiUrl = "http://54.213.89.58:9000/api/v1/";
        const string authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTMsImVtYWlsIjoibXppcGFnYW5nQGJvbGR0eXBlLmNvbSIsInJvbGUiOiJBZG1pbiIsInBlcm1pc3Npb25zIjpudWxsLCJpYXQiOjE1ODU4MTUxMzB9.xQSoWH2eZ4R8X2z-_fGZ_AtaWy0g6HRQs9k9D5qp5uI";

        const int tempMin = 75;
        const int tempMax = 77;
        const int windSpeedMin = 3;
        const int windSpeedMax = 5;
        const int windDirectionMin = 180;
        const int windDirectionMax = 210;
        const int fireDelta = 10;
        const int normal_sleep = 10000;
        const int fire_sleep = 3000;

        static HashSet<int> hasFire = new HashSet<int>();

        static async Task Main(string[] args)
        {
            StartNormalLoop();
            Console.WriteLine($"Starting data streams...");

            var sites = await GetSites();

            while(true) {
                Console.WriteLine("Select site:");
                foreach (dynamic site in sites) {
                    Console.WriteLine($"{site.id} - {site.name}");
                }
                var key = Console.ReadLine();
                if (key == "q") {
                    Console.WriteLine("\nStopping fires...");
                    hasFire.Clear();
                } else if (int.Parse(key) > 0) {
                    Console.WriteLine("\nSimulating fire...");
                    StartFire(int.Parse(key)); 
                }
            }
        }

        private static async void StartFire(int siteId)
        {
            var devices = await GetDevicesBySite(siteId);
            foreach (dynamic device in devices) {
                hasFire.Add((int)device.id);
            }
        }

        private static async void StartNormalLoop()
        {
            var devices = await GetAllDevices();
            foreach (dynamic device in devices) {
                Thread sender = new Thread(SendData);
                sender.IsBackground = true;
                sender.Start(device);
            }
        }

        private static async void SendData(dynamic device)
        {
            while(true) 
            {
                var ts = DateTimeOffset.Now.ToUnixTimeSeconds();
                var rand = new Random((int)ts);

                var tempWhole = rand.Next(tempMin, tempMax);
                var tempFraction = rand.NextDouble();

                var windSpeedWhole = rand.Next(windSpeedMin, windSpeedMax);
                var windSpeedFraction = rand.NextDouble();

                var windDirection = rand.Next(windDirectionMin, windDirectionMax);
                
                var sensorData = new {
                    deviceId = device.id,
                    timestamp = ts,
                    temp = tempWhole + tempFraction + (hasFire.Contains((int)device.id) ? fireDelta : 0),
                    windSpeed = windSpeedWhole + windSpeedFraction + (hasFire.Contains((int)device.id) ? fireDelta : 0),
                    windDirection = windDirection
                };
                var sensorJson = new StringContent(JsonConvert.SerializeObject(sensorData), Encoding.UTF8, "application/json");

                using(var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                    await client.PostAsync($"{apiUrl}sensordata", sensorJson);
                };
                (HttpStatusCode code, string bucket) = await Upload($"{device.id}-{ts}.png", $"photos/{(hasFire.Contains((int)device.id) ? "fire" : device.id)}.png");
                if(code == HttpStatusCode.OK) {
                    var photoData = new {
                        deviceId = device.id,
                        timestamp = ts
                    };
                    var photoJson = new StringContent(JsonConvert.SerializeObject(photoData), Encoding.UTF8, "application/json");

                    using(var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                        await client.PostAsync($"{apiUrl}photo", photoJson);
                    };
                }

                Thread.Sleep(hasFire.Contains((int)device.id) ? fire_sleep : normal_sleep);
            }
        }

        private static async Task<dynamic> GetSites()
        {
            dynamic sites;
            using(var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                var result = await client.GetStringAsync($"{apiUrl}site/0/12");
                sites = JArray.Parse(result);
            }
            return sites;
        }

        private static async Task<dynamic> GetAllDevices()
        {
            dynamic devices;
            using(var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                var result = await client.GetStringAsync($"{apiUrl}device/0/36");
                devices = JArray.Parse(result);
            }
            return devices;
        }

        private static async Task<dynamic> GetDevicesBySite(int siteId)
        {
            dynamic devices;
            using(var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                var result = await client.GetStringAsync($"{apiUrl}device/site/{siteId}");
                devices = JArray.Parse(result);
            }
            return devices;
        }

        private static async Task<dynamic> GetSTSToken(string filename)
        {
            dynamic token;
            using(var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
                var result = await client.GetStringAsync($"{apiUrl}photo/{filename}/token");
                token = JValue.Parse(result);
            }
            return token;
        }

        private static async Task<(HttpStatusCode, string)> Upload(string filename, string path)
        {
            try
            {
                dynamic token = await GetSTSToken(filename);
                var sessionCredentials = new SessionAWSCredentials(
                    (string)token.awsKey,
                    (string)token.secretKey,
                    (string)token.sessionToken
                );
                using (var s3Client = new AmazonS3Client(sessionCredentials, RegionEndpoint.USWest2))
                {
                    PutObjectRequest putObjectRequest = new PutObjectRequest
                    {
                        BucketName = (string)token.bucket,
                        Key = filename,
                        FilePath = path,
                        ContentType = "image/png"
                    };
                    putObjectRequest.CannedACL = S3CannedACL.PublicRead;
                    PutObjectResponse response = await s3Client.PutObjectAsync(putObjectRequest);
                    return (response.HttpStatusCode, token.bucket);
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                Console.WriteLine(s3Exception.Message, s3Exception.InnerException);
                return (HttpStatusCode.BadRequest, null);
            }
            catch (AmazonSecurityTokenServiceException stsException)
            {
                Console.WriteLine(stsException.Message, stsException.InnerException);
                return (HttpStatusCode.BadRequest, null);
            }
        }
    }
}
